﻿using Effort;
using NFluent;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace poc_ef6.pocs.OneToMany
{
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Lot> Lots { get; set; }
    }

    class Lot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }

    class MyContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Lot> Lots { get; set; }

        public MyContext(DbConnection connection) : base(connection, false) { }
    }

    public class Tests
    {
        DbConnection connection;
        public Tests()
        {
            connection = DbConnectionFactory.CreateTransient();
        }

        [Fact]
        public void SaveUserWithLots()
        {
            using (var context = new MyContext(connection))
            {
                var user = new User()
                {
                    Name = "Sergio",
                    Lots = new List<Lot>
                    {
                        new Lot() { Name = "Lot-1"},
                        new Lot() { Name = "Lot-2"},
                        new Lot() { Name = "Lot-3"},
                    }
                };

                Check.That(user.Id).IsZero();
                Check.That(user.Lots.First().UserId).IsZero();
                Check.That(user.Lots.First().User).IsNull();

                context.Users.Add(user);
                context.SaveChanges();

                Check.That(user.Id).IsStrictlyPositive();
                Check.That(user.Lots.First().UserId).IsEqualTo(user.Id);
                Check.That(user.Lots.First().User).IsSameReferenceAs(user);
            }

            using (var context = new MyContext(connection))
            {
                var user = context.Users.First(u => u.Name == "Sergio");
                Check.That(user.Lots).IsNull();
            }

            using (var context = new MyContext(connection))
            {
                var user = context.Users.Include(u => u.Lots).First(u => u.Name == "Sergio");
                Check.That(user.Lots).HasSize(3);
                Check.That(user.Lots.Extracting(nameof(Lot.Name))).ContainsExactly("Lot-1", "Lot-2", "Lot-3");
            }

            using (var context = new MyContext(connection))
            {
                var user = context.Users.First(u => u.Name == "Sergio");
                // force loading related lots in the context
                context.Lots.Where(lot => lot.UserId == user.Id).ToList();
                Check.That(user.Lots).HasSize(3);
                Check.That(user.Lots.Extracting(nameof(Lot.Name))).ContainsExactly("Lot-1", "Lot-2", "Lot-3");
            }

            using (var context = new MyContext(connection))
            {
                var user = context.Users.First(u => u.Name == "Sergio");
                // loading only ONE related lot in the context
                context.Lots.First(lot => lot.UserId == user.Id);
                Check.That(user.Lots).HasSize(1);
                Check.That(user.Lots.Extracting(nameof(Lot.Name))).ContainsExactly("Lot-1");
                user.Lots.Add(new Lot() { Name = "Lot-4" });

                context.SaveChanges();
            }

            using (var context = new MyContext(connection))
            {
                var user = context.Users.First(u => u.Name == "Sergio");
                context.Lots.Where(lot => lot.UserId == user.Id).ToList();
                Check.That(user.Lots).HasSize(4);
                Check.That(user.Lots.Extracting(nameof(Lot.Name))).ContainsExactly("Lot-1", "Lot-2", "Lot-3", "Lot-4");

                context.Lots.Remove(user.Lots.First());
                context.SaveChanges();
            }

            using (var context = new MyContext(connection))
            {
                var user = context.Users.First(u => u.Name == "Sergio");
                context.Lots.Where(lot => lot.UserId == user.Id).ToList();
                Check.That(user.Lots).HasSize(3);
                Check.That(user.Lots.Extracting(nameof(Lot.Name))).ContainsExactly("Lot-2", "Lot-3", "Lot-4");
            }


        }

        [Fact]
        public void SaveLotsWithUser()
        {
            using (var context = new MyContext(connection))
            {
                var user = new User() { Name = "Sergio" };
                var lot1 = new Lot() { Name = "Lot-1", User = user };
                var lot2 = new Lot() { Name = "Lot-2", User = user };

                Check.That(user.Lots).IsNull();

                context.Lots.Add(lot1);
                context.Lots.Add(lot2);
                context.SaveChanges();

                Check.That(user.Lots).HasSize(2);
            }
        }
    }

    //public class Program
    //{
    //    /*static */
    //    public void Main()
    //    {
    //        CreateOneUserWithThreeLots();
    //        Console.WriteLine("Done");
    //        Console.ReadKey();
    //    }

    //    static void CreateOneUserWithThreeLots()
    //    {
    //        using (var context = new MyContext())
    //        {
    //            var user = new User()
    //            {
    //                Name = "Sergio",
    //                Lots = new List<Lot>
    //                {
    //                    new Lot() { Name = "Lot-1"},
    //                    new Lot() { Name = "Lot-2"},
    //                    new Lot() { Name = "Lot-3"},
    //                }
    //            };

    //            context.Users.Add(
    //                new User()
    //                {
    //                    Name = "Sergio",
    //                    Lots = new List<Lot>
    //                    {
    //                        new Lot() { Name = "Lot-1"},
    //                        new Lot() { Name = "Lot-2"},
    //                        new Lot() { Name = "Lot-3"},
    //                    }
    //                }
    //            );
    //            context.SaveChanges();
    //        }

    //        using (var context = new MyContext())
    //        {
    //            var user = context.Users
    //                .Include(it => it.Lots)
    //                .First(it => it.Name == "Sergio");
    //        }

    //        using (var context = new MyContext())
    //        {
    //            var user = context.Users
    //                .First(it => it.Name == "Sergio");

    //            Console.WriteLine("user lots:", user.Lots.Count());
    //        }
    //    }
    //}
}
