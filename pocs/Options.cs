﻿using Effort;
using NFluent;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using Xunit;

namespace poc_ef6.pocs
{
    interface IOptionContainer
    {
        IEnumerable<Option> GetAllOptions();
    }

    interface IOptionValueContainer
    {
        IEnumerable<OptionValue> GetAllValues();
    }


    class Option : IOptionContainer, IOptionValueContainer
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public OptionValue Value { get; set; }

        public int LotId { get; set; }
        public Lot Lot { get; set; }

        public Option() { }
        public Option(string key, OptionValue value)
        {
            Key = key;
            Value = value;
        }

        public IEnumerable<Option> GetAllOptions()
        {
            yield return this;
            foreach (var option in Value.GetAllOptions())
            {
                yield return option;
            }
        }

        public IEnumerable<OptionValue> GetAllValues()
        {
            return Value.GetAllValues();
        }
    }

    abstract class OptionValue : IOptionContainer, IOptionValueContainer
    {
        public int Id { get; set; }

        public int LotId { get; set; }
        public Lot Lot { get; set; }

        // REMARK: object are not mapped by EF6
        public abstract object RawData { get; set; }

        public virtual OptionValue this[int index]
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        public virtual OptionValue this[string key]
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        [NotMapped]
        public virtual string AsString
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        [NotMapped]
        public virtual double AsNumber
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        [NotMapped]
        public virtual List<OptionValue> AsValueList
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        [NotMapped]
        public virtual List<Option> AsOptionList
        {
            get => throw new Exception();
            set => throw new Exception();
        }

        public virtual IEnumerable<OptionValue> GetAllValues()
        {
            yield return this;
        }

        public virtual IEnumerable<Option> GetAllOptions()
        {
            return Enumerable.Empty<Option>();
        }
    }

    class StringValue : OptionValue
    {
        public override object RawData
        {
            get => Data;
            set => Data = (string)value;
        }

        [Column("String")]
        public string Data { get; set; }

        public StringValue() { }
        public StringValue(string value)
        {
            Data = value;
        }

        [NotMapped]
        public override string AsString
        {
            get => Data;
            set => Data = value;
        }
    }

    class NumericValue : OptionValue
    {
        public override object RawData
        {
            get => Data;
            set => Data = (double)value;
        }

        [Column("Numeric")]
        public double Data { get; set; }

        public NumericValue() { }
        public NumericValue(double value)
        {
            Data = value;
        }

        [NotMapped]
        public override double AsNumber
        {
            get => Data;
            set => Data = value;
        }

    }

    class ValueCollection : OptionValue
    {
        public override object RawData
        {
            get => Data;
            set => Data = (List<OptionValue>)value;
        }

        public List<OptionValue> Data { get; set; }

        public ValueCollection() { }

        public ValueCollection(params OptionValue[] values)
        {
            Data = new List<OptionValue>(values);
        }

        public override List<OptionValue> AsValueList { get => Data; set => Data = value; }

        public override OptionValue this[int index] => Data[index];

        public override IEnumerable<OptionValue> GetAllValues()
            => base.GetAllValues().Concat(
                    Data.SelectMany(optionValue => optionValue.GetAllValues()));

        public override IEnumerable<Option> GetAllOptions()
            => Data.SelectMany(optionValue => optionValue.GetAllOptions());

    }

    class OptionCollection : OptionValue
    {
        public override object RawData
        {
            get => Data;
            set => Data = (List<Option>)value;
        }

        public List<Option> Data { get; set; }

        public OptionCollection() { }

        public OptionCollection(params Option[] options)
        {
            Data = new List<Option>(options);
        }

        public override OptionValue this[string key]
            => Data?.First(option => option.Key == key)?.Value;

        public override IEnumerable<OptionValue> GetAllValues()
            => base
                .GetAllValues()
                .Concat(Data.SelectMany(option => option.Value.GetAllValues()));

        public override IEnumerable<Option> GetAllOptions()
            => Data.Concat(Data.SelectMany(option => option.Value.GetAllOptions()));

        [NotMapped]
        public override List<Option> AsOptionList
        {
            get => Data;
            set => Data = value;
        }
    }

    class Lot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public OptionCollection Options { get; set; }

        public ICollection<OptionValue> AllOptionValues { get; set; }
        public ICollection<Option> AllOptions { get; set; }
    }


    class MyContext : DbContext
    {
        public DbSet<Lot> Lots { get; set; }
        public DbSet<OptionValue> OptionValues { get; set; }
        public DbSet<Option> Options { get; set; }

        public MyContext()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<MyContext>());
            Database.Log = s => Debug.WriteLine(s);
        }

        public MyContext(DbConnection connection) : base(connection, false)
        {
            Database.Log = s => Debug.WriteLine(s);
        }
    }

    public class OptionTests
    {
        readonly DbConnection connection;

        public OptionTests()
        {
            connection = DbConnectionFactory.CreateTransient();

            CreateOneLotWithSeveralOptions();
        }

        [Fact]
        public void ReloadingLotDoesNotIncludeDataByDefault()
        {
            using (var context = new MyContext(connection))
            {
                var reloadedLot = context.Lots.First(lot => lot.Name == "lot-1");

                Check.That(reloadedLot).IsNotNull();
                Check.That(reloadedLot.Options).IsNull();
            }
        }

        [Fact]
        public void UseIncludeExlicitelyToGetOptions()
        {
            using (var context = new MyContext(connection))
            {
                var reloadedLot = context.Lots
                    .Include(lot => lot.Options.Data.Select(option => option.Value))
                    .First(lot => lot.Name == "lot-1");

                Check.That(reloadedLot.Options).IsNotNull();
                Check.That(reloadedLot.Options.Data).IsNotNull();
                Check.That(reloadedLot.Options["Aperture"].AsNumber).IsEqualTo(6);
                Check.That(reloadedLot.Options["Manufacturer"].AsString).IsEqualTo("Lambda-X");
                Check.That(reloadedLot.Options["OtherNames"].RawData).IsNull();
            }
        }

        [Fact]
        public void DealingWithNestedLevels()
        {
            using (var context = new MyContext(connection))
            {
                var reloadedLot = context.Lots.First(lot => lot.Name == "lot-1");

                // currently, options is not accessible because option values are not loaded in context.
                Check.That(reloadedLot.Options).IsNull();

                var allOptionValues = context.OptionValues.ToList();
                // once optionValues loaded, the lot options is not null
                Check.That(reloadedLot.Options).IsNotNull();
                // but as there are no options lodade in context, we can not go further in the option hierarchy
                Check.That(reloadedLot.Options.Data).IsNull();
                Check.That(reloadedLot.Options["OtherNames"]).IsNull();

                var allOptions = context.Options.ToList();
                // once options are loaded, we can navigate through the hierararchy at several level
                Check.That(reloadedLot.Options["OtherNames"][3]["Deep-inside1"].RawData).IsEqualTo("one");
            }
        }

        [Fact]
        public void UsingRootOptionsToReloadOnlyWhatIsNeeded()
        {
            using (var context = new MyContext(connection))
            {
                var reloadedLot = context.Lots
                    .Include(lot => lot.AllOptions)
                    .Include(lot => lot.AllOptionValues)
                    .First(lot => lot.Name == "lot-1");

                // once options are loaded, we can navigate through the hierararchy at several level
                Check.That(reloadedLot.Options["OtherNames"][3]["Deep-inside1"].RawData).IsEqualTo("one");

                Check.That(reloadedLot.AllOptions).ContainsOnlyElementsThatMatch(option => option.Lot == reloadedLot);
                Check.That(reloadedLot.AllOptions).ContainsOnlyElementsThatMatch(option => option.LotId == reloadedLot.Id);
                Check.That(reloadedLot.AllOptionValues).ContainsOnlyElementsThatMatch(value => value.Lot == reloadedLot);
                Check.That(reloadedLot.AllOptionValues).ContainsOnlyElementsThatMatch(value => value.LotId == reloadedLot.Id);
            }
        }

        [Fact]
        public void GetOptionWithoutLot()
        {
            using (var context = new MyContext(connection))
            {
                var option = context.Options.First();
                Check.That(option.LotId).IsNotZero();
                Check.That(option.Lot).IsNull();
            }
        }

        [Fact]
        public void DeletingOptionValueAtTheDeepestLevel()
        {
            int initialValueCount = -1;
            using (var context = new MyContext(connection))
            {
                initialValueCount = context.OptionValues.Count();
                var reloadedLot = context.Lots
                    .Include(lot => lot.AllOptions)
                    .Include(lot => lot.AllOptionValues)
                    .First(lot => lot.Name == "lot-1");

                var optionValue = reloadedLot.Options["OtherNames"][3]["Deep-inside1"];
                reloadedLot.Options["OtherNames"].AsValueList.Remove(optionValue);
                reloadedLot.AllOptionValues.Remove(optionValue);
                optionValue.Lot = null;
                context.OptionValues.Remove(optionValue);
                context.SaveChanges();
            }
            using (var context = new MyContext(connection))
            {
                Check.That(context.OptionValues).HasSize(initialValueCount - 1);
            }
        }

        [Fact]
        public void DeletingIntegralValueAtFirstLevel()
        {
            int initialValueCount;
            int initialOptionCount;

            using (var context = new MyContext(connection))
            {
                initialValueCount = context.OptionValues.Count();
                initialOptionCount = context.Options.Count();
            }

            DeleteOptionByKey("a-numeric");

            using (var context = new MyContext(connection))
            {
                Check.That(context.Options).HasSize(initialOptionCount - 1);
                Check.That(context.OptionValues).HasSize(initialValueCount - 1);
            }
        }

        [Fact]
        public void DeletingListAtFirstLevel()
        {
            int initialValueCount;
            int initialOptionCount;

            using (var context = new MyContext(connection))
            {
                initialValueCount = context.OptionValues.Count();
                initialOptionCount = context.Options.Count();
            }

            DeleteOptionByKey("a-list");

            using (var context = new MyContext(connection))
            {
                Check.That(context.Options).HasSize(initialOptionCount - 1);
                Check.That(context.OptionValues).HasSize(initialValueCount - 4);
            }
        }

        [Fact]
        public void DeletingListAtSecondLevel()
        {
            int initialValueCount;
            int initialOptionCount;

            using (var context = new MyContext(connection))
            {
                initialValueCount = context.OptionValues.Count();
                initialOptionCount = context.Options.Count();
            }

            DeleteOptionByKey("sub-list");

            using (var context = new MyContext(connection))
            {
                Check.That(context.Options).HasSize(initialOptionCount - 1);
                Check.That(context.OptionValues).HasSize(initialValueCount - 4);
            }
        }

        [Fact]
        public void DeletingOptionCollectionAtFirstLevel()
        {
            int initialValueCount;
            int initialOptionCount;

            using (var context = new MyContext(connection))
            {
                initialValueCount = context.OptionValues.Count();
                initialOptionCount = context.Options.Count();
            }

            DeleteOptionByKey("sub-options");

            using (var context = new MyContext(connection))
            {
                Check.That(context.Options).HasSize(initialOptionCount - 5);
                Check.That(context.OptionValues).HasSize(initialValueCount - 8);
            }
        }

        private void DeleteOptionByKey(string key)
        {
            using (var context = new MyContext(connection))
            {
                var optionToDelete = context.Options
                    .Include(option => option.Lot.AllOptions)
                    .Include(option => option.Lot.AllOptionValues)
                    .Where(option => option.Key == key).AsEnumerable().Single();

                var allOptionsToDelete = optionToDelete.GetAllOptions().ToList();
                var allValuesToDelete = optionToDelete.Value.GetAllValues().ToList();

                var lot = optionToDelete.Lot;

                lot.AllOptions = lot.AllOptions.Where(option => !allOptionsToDelete.Contains(option)).ToList();
                lot.AllOptionValues = lot.AllOptionValues.Where(value => !allValuesToDelete.Contains(value)).ToList();
                lot.Options.AsOptionList.Remove(optionToDelete);

                allOptionsToDelete.ForEach(option =>
                {
                    option.Lot = null;
                    option.Value = null;
                    context.Options.Remove(option);
                });
                allValuesToDelete.ForEach(value =>
                {
                    value.Lot = null;
                    context.OptionValues.Remove(value);
                });

                context.SaveChanges();
            }
        }


        private void CreateOneLotWithSeveralOptions()
        {
            using (var context = new MyContext(connection))
            {
                var lot = new Lot()
                {
                    Name = "lot-1",
                };

                context.Lots.Add(lot);
                context.SaveChanges();
                lot.Options = new OptionCollection(
                    new Option("a-numeric", new NumericValue(1234)),
                    new Option("a-string", new StringValue("the-string-value")),
                    new Option("a-list", new ValueCollection(
                        new StringValue("list-item-1"),
                        new StringValue("list-item-2"),
                        new StringValue("list-item-3"))),
                    new Option("sub-options", new OptionCollection(
                        new Option("sub-1", new StringValue("sub-value-1")),
                        new Option("sub-2", new StringValue("sub-value-2")),
                        new Option("sub-3", new StringValue("sub-value-3")),
                        new Option("sub-list", new ValueCollection(
                            new StringValue("sub-list-item-1"),
                            new StringValue("sub-list-item-2"),
                            new StringValue("sub-list-item-3"))))),
                    new Option("Aperture", new NumericValue(6)),
                    new Option("Manufacturer", new StringValue("Lambda-X")),
                    new Option("OtherNames", new ValueCollection(
                        new StringValue("hello"),
                        new StringValue("world"),
                        new StringValue("foo"),
                        new OptionCollection(
                                new Option("Deep-inside1", new StringValue("one")),
                                new Option("Deep-inside2", new StringValue("two")),
                                new Option("Deep-inside3", new StringValue("three"))))));

                lot.Options.GetAllValues().ToList().ForEach(value => value.Lot = lot);
                lot.Options.GetAllOptions().ToList().ForEach(option => option.Lot = lot);

                // the options are not automatically added to context while not saved
                Check.That(context.OptionValues).IsEmpty();
                context.SaveChanges();

                // once saved, the option values are part of the context.
                Check.That(context.OptionValues).HasSize(25);

                // once saved, the lot and the options have an id
                Check.That(lot.Id).IsNotZero();
                Check
                    .That(context.OptionValues.Extracting(nameof(OptionValue.Id)))
                    .ContainsOnlyElementsThatMatch(id => (int)id > 0);

                lot.AllOptionValues = lot.Options.GetAllValues().ToList();
                lot.AllOptions = lot.Options.GetAllOptions().ToList();

                Check.That(lot.AllOptionValues).HasSize(25);
                Check.That(lot.AllOptions).HasSize(14);
                context.SaveChanges();
            }
        }
    }
}
