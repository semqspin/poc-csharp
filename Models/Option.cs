﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_ef6.Models
{
    public class Option
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public IOptionValue Value { get; set; }

        public Option() { }

        public Option(string key, IOptionValue value)
        {
            Key = key;
            Value = value;
        }

        public override string ToString()
        {
            return $"{Key}: {Value}";
        }
    }
}
