﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_ef6.Models
{
    public class NumericOptionValue : IOptionValue
    {
        [Column("Numeric")]
        public int Data { get; set; }

        public NumericOptionValue() { }

        public NumericOptionValue(int value)
        {
            Data = value;
        }

        public override string ToString()
        {
            return Data.ToString();
        }

    }
}
