﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_ef6.Models
{
    public class ListOfOptions : IOptionValue
    {
        public List<Option> Data { get; set; }

        public override string ToString()
        {
            return Data == null ? "null" : $"[{string.Join(",", Data)}]";
        }
    }
}
