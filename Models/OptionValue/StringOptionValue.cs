﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_ef6.Models
{
    public class StringOptionValue : IOptionValue
    {
        [Column("String")]
        public string Data { get; set; }

        public StringOptionValue() { }

        public StringOptionValue(string value)
        {
            this.Data = value;
        }

        public override string ToString()
        {
            return Data; ;
        }

    }
}
