﻿using poc_ef6.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_ef6.DAL
{
    public class MyContext : DbContext
    {
        public DbSet<Option> Options { get; set; }
        public DbSet<IOptionValue> OptionValues { get; set; }

        public MyContext()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<MyContext>());
            // Database.Log = Console.Write;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
