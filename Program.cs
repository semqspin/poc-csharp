﻿using poc_ef6.DAL;
using poc_ef6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace poc_ef6
{
    class Program
    {
        static void Main(string[] args)
        {
            //createOptions();
            //readOptions();
            Console.WriteLine("Done");
            Console.ReadKey();
        }

        static void readOptions()
        {
            using (var context = new MyContext())
            {
                var options = context.Options.Include(o => o.Value).ToList();
                Console.WriteLine(string.Join(Environment.NewLine, options));

            }

        }

        static void createOptions()
        {
            var options = new List<Option>() {
                new Option("Name", new StringOptionValue("first-lot")),
                new Option("Aperture", new NumericOptionValue(123)),
                new Option("Several", new ListOfOptionValues()
                {
                    Data = new List<IOptionValue>()
                    {
                        new NumericOptionValue(456),
                        new NumericOptionValue(789),
                        new StringOptionValue("We can mix"),
                    }
                }),
                new Option("SubOptions", new ListOfOptions()
                {
                    Data = new List<Option>()
                    {
                        new Option("sub-one", new StringOptionValue("some-value-for-sub-one")),
                        new Option("sub-two", new NumericOptionValue(987)),
                        new Option("sub-three", new ListOfOptionValues()
                        {
                            Data = new List<IOptionValue>()
                            {
                                new NumericOptionValue(12),
                                new StringOptionValue("sub-sub-sub..."),
                            }
                        }),
                    }
                }),
            };

            using (var ctx = new MyContext())
            {

                ctx.Options.AddRange(options);
                ctx.SaveChanges();
            }

        }
    }
}
